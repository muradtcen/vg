<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190720170304 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $authorsQuery = file_get_contents('src/resources/dump_authors.sql');
        $this->addSql($authorsQuery);

        $worksQuery = file_get_contents('src/resources/dump_works.sql');
        $this->addSql($worksQuery);

        $categoriesQuery = file_get_contents('src/resources/dump_categories.sql');
        $this->addSql($categoriesQuery);

        $categoriesWorksQuery = file_get_contents('src/resources/dump_categories_works.sql');
        $this->addSql($categoriesWorksQuery);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
