<?php

namespace App\Controller;

use App\Entity\Work;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WorkController extends AbstractController
{
    /**
     * @Route("/api/work", name="work",methods="GET")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $works = $entityManager->getRepository(Work::class)->findAll();

        return $works;
    }
}
