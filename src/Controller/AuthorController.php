<?php

namespace App\Controller;

use App\Entity\Author;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController
{
    /**
     * @Route("/api/author", name="author",methods="GET")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $authors = $entityManager->getRepository(Author::class)->findAll();

        return $this->json($authors);
    }


    /**
     * @Route("/api/author", methods="POST")
     * @param Request $request
     * @param AuthorRepository $movieRepository
     * @param EntityManagerInterface $em
     * @return
     */
    public function create(Request $request, AuthorRepository $movieRepository, EntityManagerInterface $em)
    {
        $request = $this->transformJsonBody($request);

        $author = new Author;
        $author->setTitle($request->get('title'));
        $em->persist($author);
        $em->flush();

        return $this->respondCreated($movieRepository->transform($author));
    }
}
