<?php

namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/api/category", name="category",methods="GET")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $categories = $entityManager->getRepository(Category::class)->findAll();

        return  $categories;
    }
}
